<?
namespace GlobalApp;
/**
* базовый класс для всех контроллеров
*/
abstract class BaseCont {
    
    /**
    * Метод для установки параметров 
    * @param array $params[
    *        "templateName"=>string// строка и именем шаблона, который надо подключить
    *        "params"=>array// ассоциативный массив с параметрами, которые будем использовать
    *    ]
    */
    abstract function setParams($arParams);
    
    /**
    * Метод, в котором реализуем исполнение компонента
    */
    abstract function execComp();
    
    /**
    * Метод вызывающий шаблон
    * @param array $arResult это какой то массив, который вычислим в execComp()
    */
    abstract function getView($arResult);
    
    
    
}

