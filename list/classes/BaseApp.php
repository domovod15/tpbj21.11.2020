<?
namespace GlobalApp;

use \UsersTable;
/**
* Базовый класс приложения.
* 1. Устанавливает соединение с бд
* 2. Помогает запускать компоненты
*/
class BaseApp {
    
    //ссылка на  текущее соединение с бд
    var $dbLink=false;
    
    //базовая папка приложения
    private $rootDir="";
    //базовая папка моделей
    private $modelDir="";
    //базовая папка контроллеров
    private $contDir="";
    //базовая папка настроек
    private $setDir="";
    //базовая папка шаблонов
    private $viewDir="";
    //сюда поместим класс с данными для подключения к бд
    private $dbParams=false;
    //флаг показывающий, что настройки приложения еще не загружались
    private $loadingSetComplited=false;
    //информация о текущем пользователе, если такой будет
    protected $userInfo=false;
    
    function __construct(){
        
        include_once(__DIR__."/ErrorsCodes.php");
        include_once(__DIR__."/BaseCont.php");
        include_once(__DIR__."/../model/BaseMysql.php");
        
        $this->rootDir=__DIR__."/../";
        $this->modelDir=__DIR__."/../model/";
        $this->contDir=__DIR__."/../cont/";
        $this->viewDir=__DIR__."/../view/";
        $this->setDir=__DIR__."/../set/";
        
        
    }
    
    /**
    * Метод вызывает функции установкщики настроек
    */
    function loadSettingApp(){
        
        if(!$this->loadingSetComplited){
            //настройки бд
            $this->loadSettingDb();
            //проверка пользователя
            $this->getExtractUserInfo();
        }
        
        $this->loadingSetComplited=true;
        
    }
    
    /**
    * Метод загружает настройки базы данных
    */
    final private function loadSettingDb(){
        
        if(is_file($this->setDir."setting.php")){
            
            include_once($this->setDir."setting.php");
            
            
            if(is_object($paramsData)){
                
                
                //проверяем параметры для бд
                if(
                    $paramsData->db_host&&
                    $paramsData->db_name&&
                    $paramsData->db_user&&
                    $paramsData->db_pass
                ){
                    
                    $this->dbParams=$paramsData;
                    
                    //устанавливаем соединение с бд
                    //объектно-ориентированный стиль (рекомендуется)
                    $dbLink = new \mysqli($paramsData->db_host,$paramsData->db_user,$paramsData->db_pass,$paramsData->db_name);
                    
                    
                    if ($dbLink->connect_error) {
                        
                        $this->includeComponent("view_base_error",$templateName="view_base_error",["CODE"=>4,"MORE_INFO"=>'Error : ('. $dbLink->connect_errno .') '. $dbLink->connect_error]);
                        die();
                        
                    }
                    else
                    {
                        $this->dbLink=$dbLink ;
                    }
                    
                }
                else
                {
                    $this->includeComponent("view_base_error",$templateName="view_base_error",["CODE"=>3]);
                    die();
                }
             
                
            }
            else
            {
                $this->includeComponent("view_base_error",$templateName="view_base_error",["CODE"=>2]);
                die();
            }  
            
            
        }
        else
        {
            $this->includeComponent("view_base_error",$templateName="view_base_error",["CODE"=>1]);
            die();
        }
    }
    
    /**
    * Метод позволяет вывывать нужные компоненты передавая в них массив с параметрами
    * @param string $name имя компонента/контроллера 
    * @param string $templateName имя шаблона компонента/контроллера 
    * @param array $params список параметров компонента/контроллера 
    * @return string полное имя класса
    */
    function includeComponent($name,$templateName="",$params){
        
        //в данном приложении каждый компонент это небольшой контроллер над подчиненной моделью. 
        // передаем в него параметры для запуска, а дальше он их обрабатывает так, как это требуется для модели
        // модель ничего кроме получения и отправки данных не делает.
        
        
        if(is_dir($this->contDir.$name)){
            
            
            include_once($this->contDir.$name."/cont.php");
            
            //в  setParams($params) контроллера надо передать шаблон для отображения и список параметров
            //setParams(["templateName"=>$templateName,"params"=>["PARAM1"=>"","PARAM2"=>"","PARAMN"=>""]])
            
            //Получаем пространство имен
            $namespaceCont=$this->parceNamespace($name);
            
            $class="$namespaceCont\Controller"; 
            
            $componentClass=new $class();
            
            $componentClass->setParams([
                "templateName"=>$templateName,
                "params"=>$params,
            ]);
            
            $componentClass->execComp();
            
            return $class;
            
        }
        
        
    }
    
    /**
    * Метод позволяет подлючать нужный файл модели
    * @param string $name имя модели(класса)
    * @return bool
    */
    function includeModel($name){
        //здесь не будет вложенности, поэтому классы будем грузить по простому
        if(is_file($this->modelDir.$name.".php")){
            include_once($this->modelDir.$name.".php");
            return true;
        }
       
        return false;
    }
    
    /**
    * парсит имя контроллера для получения неймспейса 
    * @param string $contName имя контроллера(компонента)
    */
    function parceNamespace($contName){
        
        if($contName){
            
            $str=str_replace(" ","",(ucwords(str_replace("_"," ",strtolower($contName)))));
            return $str;
        }
        
        return false;
        
    }
    
    
    
    /**
    * Функция получения базовой папки приложения
    */
    function getRootDir(){
        return $this->rootDir;
    }
    
    /**
    * Функция получения базовой папки моделей
    */
    function getModelDir(){
        return $this->modelDir;
    }
    
    /**
    * Функция получения базовой папки контроллеров
    */
    function getContDir(){
        return $this->contDir;
    }
    
    /**
    * Функция получения базовой папки шаблонов
    */
    function getViewDir(){
        return $this->viewDir;
    }
    
    
    /**
    * Функция закрытия mysql соебинения
    */
    function closeMysql(){
        return $this->dbLink->close();
    }

    /**
    * Метод устанавливает пользователя, который авторизовался в приложении
    * @param array $userInfo массив с полями пользователя и бд
    */
    function setUser($userInfo){
        //просто сохраняю инфу о текущем пользователе
        $this->userInfo=$userInfo;
    }
    
    /**
    * Заканчивает авторизацию текущего пользователя
    */
    function clearUser(){
        //также просто её очищаю
        $this->userInfo=false;
    }
    
    /**
    * Возвращает поля ит  бд текущего авторизованного пользователя
    */
    function getUser(){
        return $this->userInfo;
    }
    
    /**
    * Проверяет существование авторизации на текущем пользователе и если она есть, то возвращает инфу
    */
    function getExtractUserInfo(){
        
        //используя ид пользователя и хеш находит инфу о нем в бд и помещает в приложение
        if($this->includeModel("UsersTable")){
        
            //без ид и хеш с кук и сравниваем с тем что есть в бд
            if(isset($_COOKIE["user_id"])&&isset($_COOKIE["hash"]))
            {
                $userId=$_COOKIE["user_id"];
                $hash=$_COOKIE["hash"];
                $UsersTable=new UsersTable();
                $data=$UsersTable->getList([],["ID"=>$userId,"HASH"=>$hash]);
                //если пользователя нашли, то авторизуем
                if(count($data["ITEMS"])==1){
                    $usersInfo=array_values($data["ITEMS"]);
                    $userInfo = array_shift($usersInfo);
                    $this->setUser($userInfo);
                }
                else
                {
                    $this->userInfo=false;
                }
            }
            else
            {
                $this->userInfo=false;
            }
        }
        else
        {
            $this->userInfo=false;
        }
    }
    
    
}


































