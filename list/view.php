<?
//начинаем с того, что создаем базовый объект приложения.
//Он установит соединение с бд, проставит пути, позволи в дальнейшем подключать разные контроллеры/компоненты
ini_set('error_reporting', E_ALL);
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);


global $App;
//в данном случае я использую простое подключение управляющего класса и через него буду все остальные классы притягивать.
//вообще если использовать другую структуру папопок, то можно сделать более традиционное решение по подключению классов
//путем определения автозагрузчика классов.
//сейчас же неймспейсы имеют скорее символический характер, чем реальную пользу=)
require_once(__DIR__."/classes/BaseApp.php");
use GlobalApp\BaseApp;


$App=new BaseApp();
$App->loadSettingApp();


//включаем хедер
$App->includeComponent("start_page",$templateName="start_page",[]);
?>
<div class="container">
    
    <div class="row">
        <div class="col-xs-12">
           <?
            //включаем котнроллер ответственный создание задачи
            $App->includeComponent("view_task",$templateName="view_task",[]);
           ?>
        </div>
    </div>
    
</div>

<?

//включаем footer
$App->includeComponent("end_page",$templateName="end_page",[]);
//закрываем соединение с бд
$App->closeMysql();
?>