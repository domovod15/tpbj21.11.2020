<?
namespace TaskList;

use \GlobalApp\ErrorsList;
use \GlobalApp\BaseCont;
use \TasksTable;

/** 
* Данный компонент служит для отображения списка задач
*/
class Controller extends BaseCont {
    
    function __construct (){
        
        global $App;
        
        $this->tmplPath=$App->getViewDir();
        
    }
        
    //шаблон контроллера по умолчанию
    var $templateNameDefault="task_list";
    //шаблон контроллера
    var $templateName="task_list";
    
    //режим работы r - чтение записи, w - изменение
    var $mode="r";
    /**
    * Установка параметров для использования в контроллере и шаблоне
    */
    function setParams($params=[]){     
    
        
        if($params["templateName"]){
            $this->templateName=$params["templateName"]; 
        }
        else
        {
            $this->templateName=$this->templateNameDefault;
        }
    
        if($params["params"]){
            //что то делаем с параметрами
           if(!is_array($params["params"]["SORT"])){
                $params["params"]["SORT"]=["ID"=>"ASC"];
            }
            
        }
        
        
        
        //сохраняем вошедшие параметры
        $this->params=$params["params"];
        
       
    }
    
    /**
    * исполняет контроллер и выводит шаблон
    */    
    function execComp(){
        
        global $App;
        if($App->includeModel("TasksTable")){
           
        
            //1. Отобразить список для прочения информации
            
            //соответственно так и будем делать
            $this->arResult=[];
            $this->arResult["ERRORS"]=false;
            //подготовим данные для запроса
            if(is_array($this->params["SORT"])){
                
                $arOrder=$this->params["SORT"];
                
            }
            else
            {
                $arOrder=["ID"=>"ASC"];
            }
            //теперь размер страницы
            if(intval($this->params["PAGE_SIZE"])){
                
                $limit=intval($this->params["PAGE_SIZE"]);
                
            }
            else
            {
                $limit=3;
            }
            //и сам номер страницы
            if(intval($this->params["PAGE_NUMBER"])){
                
                $page=intval($this->params["PAGE_NUMBER"]);
                
            }
            else
            {
                $page=1;
            }
            //получаем данные для отображения        
            $TasksTable=new TasksTable();
            $data=$TasksTable->getList([],[],$arOrder,$limit,$page);
            //формируем пагинацию
            $data["PAGINATION"]=$this->createPagination($data);
            //добавим параметры для сортировки
            $data["SORT_PARAM"]=$this->getSortParams();
            
            //теперь поправим ссылки на элементы
            foreach($data["ITEMS"] as &$item){
                
                $item["URL_VIEW"]=$this->getUrl2Item(1,$item["ID"]);         
            }
            
            $this->arResult["DATA"]=$data;
            
        }
        else
        {
            $arResult["ERRORS"][]=\GlobalApp\ErrorsList::getErrText(11);
        }
       
        
        
        $this->getView($this->arResult);
        
    }
    
    
    /**
    * выводит шаблон
    * @params array $arResult ассоциативный массив с данными, которые показываем в шаблоне
    */    
    function getView($arResult){
        
        //подключаем нужный шаблон и передаем в него результирующие данные
        if(is_file($this->tmplPath.$this->templateName."/template.php")){
            
            include($this->tmplPath.$this->templateName."/template.php");
        }
        
    }
    
    /**
    * Метод создает постраничную навигацию
    * @params array $data результат выборки от гетлист
    */
    function createPagination($data){
     
        if(isset($data["ROWS_IN_SELECT"])&&$data["ROWS_IN_SELECT"]>0){
            
            //по порядку берем общее количество страниц:
            $allCount=ceil($data["ROWS_IN_SELECT"]/$data["LIMIT"]);
           
            //смотрим на какой странице мы сейчас
            $actualPage=$data["PAGE"];
            //теперь подготовим предыдущую и следующую страницы
            $prevPage=$actualPage-1;
            $nextPage=$actualPage+1;
            
            
            if($prevPage<=1){
               $prevPage=false; 
            }
            if($nextPage>=$allCount){
                $nextPage=false;
            }
            //определим параметры текущей страницы
            $iPageInfo=$this->getUrl2Item(2);
            unset($iPageInfo["ARRAY"]["page_number"]);
            
            //и теперь формируем список
            //первая
            
            if(1!=$actualPage){
                $arRes["PAGES"]=[
                    "FIRST"=>[
                        "NUMBER"=>1,
                        "URL_PARAM"=>http_build_query(array_merge($iPageInfo["ARRAY"],["page_number"=>1]))
                    ],
                ];
            }
            //последняя
            if($allCount>1&&$allCount!=$actualPage){
                $arRes["PAGES"]["LAST"]=[
                    "NUMBER"=>$allCount,
                    "URL_PARAM"=>http_build_query(array_merge($iPageInfo["ARRAY"],["page_number"=>$allCount]))                
                ];
            }
            //текущая
            $arRes["PAGES"]["ACTUAL"]=[
                    "NUMBER"=>$actualPage,
                    "URL_PARAM"=>"",                    
            ];
            //предыдущая
            if($prevPage){
                $arRes["PAGES"]["PREVPAGE"]=[
                    "NUMBER"=>$prevPage,
                    "URL_PARAM"=>http_build_query(array_merge($iPageInfo["ARRAY"],["page_number"=>$prevPage]))
                ];
            }
            //следующая
            if($nextPage){
                $arRes["PAGES"]["NEXTPAGE"]=[
                    "NUMBER"=>$nextPage,
                    "URL_PARAM"=>http_build_query(array_merge($iPageInfo["ARRAY"],["page_number"=>$nextPage]))     
                ];
            }
            
            return $arRes;
            
        }
        
        return false;
    }
    
    /**
    * Метод возвращает пути для страницы
    * @param int $type вариант для которого создаем ссылку:
    *            1 - просмотр элемента,
    *            2- возвращает все параметры в для текущей страницы, для встаивания в ссылки и т.п.
    * @param int $id ид элемента для которого формируем ссылку
    * @return array  $arRes=[
    *                    "STRING"=>$baseQueryString,
    *                    "ARRAY"=>$baseGetData,
    *                ];
    */
    public function getUrl2Item($type=2,$id=false){
        
        $baseQueryString=$_SERVER['QUERY_STRING'];
        $baseGetData=$_GET;
        
        $arRes=[
            "STRING"=>$baseQueryString,
            "ARRAY"=>$baseGetData,
        ];
        
        //здесь мы смотрим какой тип запроса и в зависимости от него либо возвращаем ссылку на страницу для элемента, 
        //либо возвращаем список параметров текущей страницы для сохранения навигации и сортировки
        if($type){
            switch ($type) {
                case 1:
                    //просмотр элемента
                    //из параметров получаем ссылку на элемент
                    $PATH2VIEW=str_replace("#ID#",$id,$this->params["PATH2VIEW"]);
                    $arRes["STRING"]=$PATH2VIEW;
                    $arRes["ARRAY"]["id"]=$id;                    
                    break;
                case 2:
                    //просто в параметрах извлекаю главные для меня параметры
                    $myParam=["id","page_size","order","by","page_number"];
                    foreach($baseGetData as $k=>$v){
                        
                        if(in_array($k,$myParam)){
                            $arRes["ARRAY"][$k]=$v;
                        }
                        
                    }
                    $arRes["STRING"]=http_build_query($arRes["ARRAY"]);
                    break;
            }
            
        }
       
        
        
        return $arRes;
        
    }
    
    /**
    * Метод возвращает параметры для сортировки
    * @return array $arRes[
    *       "id"=>[
    *            "trend"=>"up",
    *            "url"=>http_build_query(array_merge($iPageInfo["ARRAY"],["by"=>"id","order"=>$order])),
    *            "actual"=>1 // только для актуального варианта сортировки
    *        ],
    *       "username"=>...,
    *       "status"=>...,
    *    ]
    */
    public function getSortParams(){
        
        
        //получаем параметры страницы
        $iPageInfo=$this->getUrl2Item(2);
        //удаляем оттуда сортировку
        unset($iPageInfo["by"],$iPageInfo["order"]);
        
        $arRes=[
            "id"=>"asc",
            "username"=>"asc",
            "status"=>"asc",
        ];
        
        //итак. Сортировка у нас в двух параметрах:
        //1. Поле       $_GET["by"]
        //2. Порядок    $_GET["order"]
        
        $order=(isset($_GET["order"]))?$_GET["order"]:"DESC";
        
        if(strtoupper($order)=="ASC"){
            $order="ASC";
            $notOrder="DESC";
        }
        else
        {
            $order="DESC";  
            $notOrder="ASC";
        }
        
        //сначала просто покажем текущий вариант сортировки
        $arRes["id"]=[
            "trend"=>($order=="DESC")?"down":"up",
            "url"=>$this->params["LOCALPATH"]."?".http_build_query(array_merge($iPageInfo["ARRAY"],["by"=>"id","order"=>$order]))
        ];
        
        $arRes["username"]=[
            "trend"=>($order=="DESC")?"down":"up",
            "url"=>$this->params["LOCALPATH"]."?".http_build_query(array_merge($iPageInfo["ARRAY"],["by"=>"username","order"=>$order]))
        ];
        
        $arRes["status"]=[
            "trend"=>($order=="DESC")?"down":"up",
            "url"=>$this->params["LOCALPATH"]."?".http_build_query(array_merge($iPageInfo["ARRAY"],["by"=>"status","order"=>$order]))
        ];
        //и теперь для выбранного поля надо сделать альтернативный и показать направление
        
        $by=(isset($_GET["by"]))?$_GET["by"]:"id";
        
        if(strtolower($by)=="id"){
            $arRes["id"]=[
                "trend"=>($notOrder=="DESC")?"up":"down",
                "url"=>$this->params["LOCALPATH"]."?".http_build_query(array_merge($iPageInfo["ARRAY"],["by"=>"id","order"=>$notOrder])),
                "actual"=>1
            ];
            
        }
        
        if(strtolower($by)=="username"){
            $arRes["username"]=[
                "trend"=>($notOrder=="DESC")?"up":"down",
                "url"=>$this->params["LOCALPATH"]."?".http_build_query(array_merge($iPageInfo["ARRAY"],["by"=>"username","order"=>$notOrder])),
                "actual"=>1
            ];
            
        }
        
        if(strtolower($by)=="status"){
            $arRes["status"]=[
                "trend"=>($notOrder=="DESC")?"up":"down",
                "url"=>$this->params["LOCALPATH"]."?".http_build_query(array_merge($iPageInfo["ARRAY"],["by"=>"status","order"=>$notOrder])),
                "actual"=>1
            ];
            
        }
        
        
        
        
        return $arRes;
        
    }
    
    
}




































