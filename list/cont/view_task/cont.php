<?
namespace ViewTask;

use \GlobalApp\ErrorsList;
use \GlobalApp\BaseCont;
use \TasksTable;

/** 
* Данный компонент служит для отображения базовых ошибок и заглушек
* 
*/
class Controller extends BaseCont {
    
    function __construct (){
        
        global $App;
        
        $this->tmplPath=$App->getViewDir();
        
    }
        
    //шаблон контроллера по умолчанию
    var $templateNameDefault="view_task";
    //шаблон контроллера
    var $templateName="view_task";
    
    //режим работы r - чтение записи, w - изменение
    var $mode="r";
    /**
    * Установка параметров для использования в контроллере и шаблоне
    */
    function setParams($params=[]){     
    
        if($params["templateName"]){
            $this->templateName=$params["templateName"]; 
        }
        else
        {
            $this->templateName=$this->templateNameDefault;
        }
    
        if($params["params"]){
            
        }
        
        //todo исправить для проверки админа
        global $App;
        
        $userInfo=$App->getUser();
        
        if(isset($userInfo["ACCESS"])){
            
            $updateFlag=(isset($_REQUEST["update"]))?$_REQUEST["update"]:"n";
            
            if(
               $userInfo["ACCESS"]=="Y"&&
               $updateFlag=="y"
            ){           
                $this->mode="w";            
            }
            
            //проверка на доступ
            $this->arResult["ACCESS"]="Y";
            
        }
        else
        {
            $this->mode="r";  
            //проверка на доступ
            $this->arResult["ACCESS"]="N";
        }
        
       
       
        //сохраняем вошедшие параметры
        $this->params=$params["params"];
       
    }
    
    /**
    * исполняет контроллер и выводит шаблон
    */    
    function execComp(){
        
        global $App;
        if($App->includeModel("TasksTable")){
           
        
            //по сути у этого контроллера всего две задачи. 
            //1. Отобразить форму для прочения информации
            //2. Отобразить форму для изменения информации
            //соответственно так и будем делать
            
            $this->arResult["ERRORS"]=false;
            $idTask=intval($_GET["id"]);
            
            $this->arResult["MODE"]=$this->mode;
            
            
            if($idTask){
                
                //сначала получим саму эту запись
                $TasksTable=new TasksTable();
                $data=$TasksTable->getList([],["ID"=>$idTask]);
                
                
                //чтение с измененением
                if($this->mode=="w"){
                    
                    //принимаем данные с запроса.
                    $email =$_REQUEST["email"];
                    $username =htmlentities(strip_tags($_REQUEST["username"]));
                    $text =htmlentities(strip_tags($_REQUEST["text"],"<br><p><ul><li><ol><b><i><u>"));
                    $status =$_REQUEST["status"];
                    
                    if($status=="C"){
                        $status="C";
                    } 
                    else{
                        $status="W";
                    }
                    
                    $errCheck=false;
                    
                    if (!filter_var($email, FILTER_VALIDATE_EMAIL)){                
                        //email bad
                        $errCheck=true;                
                        $this->arResult["ERRORS"][]=\GlobalApp\ErrorsList::getErrText(7);
                        
                    }
                    if(empty($username)){
                        $errCheck=true;                
                        $this->arResult["ERRORS"][]=\GlobalApp\ErrorsList::getErrText(8);
                    }
                    if(empty($text)){
                        $errCheck=true;                
                        $this->arResult["ERRORS"][]=\GlobalApp\ErrorsList::getErrText(9);
                    }
                
                    if(!$errCheck){
                        
                        //флаг новых данных
                        $isNew=false;
                        //флаг изменения данных админом
                        $isCheckAdmin=false;
                        
                        //надо проверить отличаются ли данные от того, что уже есть
                        
                        if($data["ITEMS"][$idTask]["TEXT"]!=$text){
                            $isNew=true;
                            $isCheckAdmin=true;
                        }
                        if($data["ITEMS"][$idTask]["EMAIL"]!=$email){
                            $isNew=true;
                        }
                        if($data["ITEMS"][$idTask]["USERNAME"]!=$username){
                            $isNew=true;
                        }
                        if($data["ITEMS"][$idTask]["STATUS"]!=$status){
                            $isNew=true;
                        }
                        
                        //ну и соответственно если есть изменения то делаем их
                        if($isNew){
                            
                            $arToSave=[               
                                'USERNAME'=>$username,
                                'EMAIL'=>$email,
                                'TEXT'=>$text,
                                'STATUS'=>$status,//W - work, C - complited
                                'CHECK_ADMIN'=>"N",//если изенились админом
                            ];
                            
                            if($isCheckAdmin){
                            $arToSave['CHECK_ADMIN']="Y";
                            }
                            
                            
                            $TasksTable=new TasksTable();
                            $resUpdate=$TasksTable->update($idTask,$arToSave);
                            $this->arResult["RES_UPD"]=$resUpdate;
                            $this->arResult["OLD_DATA"]=$data;
                            //и обновленная инфа
                            $TasksTable=new TasksTable();
                            $data=$TasksTable->getList([],["ID"=>$idTask]);
                            $this->arResult["DATA"]=$data;
                            
                        }
                        else
                        {
                            
                            $this->arResult["DATA"]=$data;
                            $this->arResult["DATA"]["ERR_MESS"]=\GlobalApp\ErrorsList::getErrText(10);
                        }
                    }       
                    
                }
                else if($this->mode=="r"){
                    $this->arResult["DATA"]=$data;
                }
                else
                {
                    $this->arResult["MODE"]=$this->mode;
                }
            }
            else
            {
                $this->arResult["ERRORS"][]=\GlobalApp\ErrorsList::getErrText(6); 
            }
        }
        else
        {
            $this->arResult["ERRORS"][]=\GlobalApp\ErrorsList::getErrText(11); 
        }
        
        
        $this->getView($this->arResult);
        
    }
    
    
    /**
    * выводит шаблон
    * @params array $arResult ассоциативный массив с данными, которые показываем в шаблоне
    */    
    function getView($arResult){
        
        //подключаем нужный шаблон и передаем в него результирующие данные
        if(is_file($this->tmplPath.$this->templateName."/template.php")){
            
            include($this->tmplPath.$this->templateName."/template.php");
        }
        
    }
    
}
