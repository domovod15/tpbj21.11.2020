<?
namespace Auth;

use \GlobalApp\ErrorsList;
use \GlobalApp\BaseCont;
use \UsersTable;

/** 
* Данный компонент служит для авторизации и окончания авторизации
*/
class Controller extends BaseCont {
    
    function __construct (){
        
        global $App;
        
        $this->tmplPath=$App->getViewDir();
        
    }
        
    //шаблон контроллера по умолчанию
    var $templateNameDefault="auth";
    //шаблон контроллера
    var $templateName="auth";
    
    /**
    * Установка параметров для использования в контроллере и шаблоне
    */
    function setParams($params=[]){     
    
        
        if($params["templateName"]){
            $this->templateName=$params["templateName"]; 
        }
        else
        {
            $this->templateName=$this->templateNameDefault;
        }
    
        if($params["params"]){
            
        }
        
        //сохраняем вошедшие параметры
        $this->params=$params["params"];
    }
    
    /**
    * исполняет контроллер и выводит шаблон
    */    
    function execComp(){
        
        global $App;
        
        $arResult=[];//результирующий  массив
        
        
        if($App->includeModel("UsersTable")){
           
            //это управление авторизацией. 
            //мы здесь либо авторизуем, либо заканчиваем авторизацию. 
            
            
            //проверим авторизован ли сейчас
            if($App->getUser())
            {
                //значит пользователь уже авторизован.
                //нам надо только показать инфу по нему
                $arResult["DATA"]=$App->getUser();
                
            }
            
            //теперь проверим, что делать 
            if(isset($_POST["auth"])){
                
                
                if($_POST["auth"]=="y"){
                    
                    
                    //делаем авторизацию
                    //инфу будем хранить в куках
                    $username =preg_replace("/^a-Z0-9/","",$_REQUEST["username"]);
                    $password =preg_replace("/^a-Z0-9/","",$_REQUEST["password"]);
                    
                    $errCheck=false;
                    if(empty($username)){                
                        //email bad
                        $errCheck=true;                
                        $arResult["ERRORS"][]=\GlobalApp\ErrorsList::getErrText(8);
                    }
                    if(empty($password)){
                        $errCheck=true;                
                        $arResult["ERRORS"][]=\GlobalApp\ErrorsList::getErrText(12);
                    }
                    
                    if(!$errCheck){
                        
                        //пробуем авторизоваться.
                        $UsersTable=new UsersTable();
                        $data=$UsersTable->getList([],["USERNAME"=>$username,"PASSWORD"=>md5($password)]);
                        //если пользователя нашли, то авторизуем
                        if(count($data["ITEMS"])==1){
                            
                            $userInfo = @array_shift(array_values($data["ITEMS"]));
                            //генерируем хеш
                            $hash=$UsersTable->getHash();
                            $userInfo["HASH"]=$hash;
                            //сохраняем хеш в базе
                            $resUpdate=$UsersTable->update($userInfo["ID"],["HASH"=>$hash]);
                            
                            //ставим пользователя в приложени
                            $App->setUser($userInfo);
                            //ставим куки на два часа
                            setcookie("user_id", $userInfo['ID'], time()+60*60*2,'/');
                            setcookie("hash", $userInfo['HASH'], time()+60*60*2,'/');
                            //даем информацию о пользователе
                            $arResult["DATA"]=$userInfo;
                            $arResult["MESS"]="Авторизация успешна";
                            
                        }
                        else
                        {
                            $arResult["ERRORS"][]=\GlobalApp\ErrorsList::getErrText(13);
                        }
                        
                        
                    }//если правильные поля
                }//если авторизация
                if($_POST["auth"]=="out"){
                    
                 
                    //просто очищаем данные
                    $arResult["DATA"]=[];
                    $arResult["MESS"]=\GlobalApp\ErrorsList::getErrText(14);
                    //и в приложении
                    $App->clearUser();
                    //и сбрасываем куки
                    setcookie("user_id", "", time()-2,'/');
                    setcookie("hash", "", time()-2,'/');
                    
                }//если закончить авторизацию
                
            }//если есть параметр авторизации
           
        }
        else
        {
            $arResult["ERRORS"][]=\GlobalApp\ErrorsList::getErrText(11);
        }
        
        
        //сохраняем и в шаблон
        $this->arResult=$arResult;
        $this->getView($this->arResult);
        
    }
    
    
    /**
    * выводит шаблон
    * @params array $arResult ассоциативный массив с данными, которые показываем в шаблоне
    */    
    function getView($arResult){
        
        //подключаем нужный шаблон и передаем в него результирующие данные
        if(is_file($this->tmplPath.$this->templateName."/template.php")){
            
            include($this->tmplPath.$this->templateName."/template.php");
        }
        
    }
    
}
