<?
namespace StartPage;

use \GlobalApp\ErrorsList;
use \GlobalApp\BaseCont;

/** 
* Данный компонент служит для отображения шапки сайта
* 
*/
class Controller extends BaseCont {
    
    function __construct (){
        
        global $App;
        
        $this->tmplPath=$App->getViewDir();
        
    }
    
    //номер ошибки устанавливается в параметрах
    var $err_no=false;
    //текст извлекается из GlobalApp\ErrorsList внетри компонента
    var $textErr=false;
    
    //шаблон контроллера по умолчанию
    var $templateNameDefault="start_page";
    //шаблон контроллера
    var $templateName="start_page";
    
    /**
    * Установка параметров для использования в контроллере и шаблоне
    */
    function setParams($params=[]){     
    
       
    }
    
    /**
    * исполняет контроллер и выводит шаблон
    */    
    function execComp(){
        
        $this->getView([]);
        
    }
    
    
    /**
    * выводит шаблон
    * @params array $arResult ассоциативный массив с данными, которые показываем в шаблоне
    */    
    function getView($arResult){
        
        //подключаем нужный шаблон и передаем в него результирующие данные
        if(is_file($this->tmplPath.$this->templateName."/template.php")){
            
            include($this->tmplPath.$this->templateName."/template.php");
        }
        
    }
    
}
