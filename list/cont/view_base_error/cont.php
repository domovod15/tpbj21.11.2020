<?
namespace ViewBaseError;

use \GlobalApp\ErrorsList;
use \GlobalApp\BaseCont;

/** 
* Данный компонент служит для отображения базовых ошибок и заглушек
* 
*/
class Controller extends BaseCont {
    
    function __construct (){
        
        global $App;
        
        $this->tmplPath=$App->getViewDir();
        
    }
    
    //номер ошибки устанавливается в параметрах
    var $err_no=false;
    //текст извлекается из GlobalApp\ErrorsList внетри компонента
    var $textErr=false;
    
    //шаблон контроллера по умолчанию
    var $templateNameDefault="view_base_error";
    //шаблон контроллера
    var $templateName="view_base_error";
    
    //массив с параметрами, которые придут при вызове контроллера
    var $params=[];
    
    /**
    * Установщик параметров для работы
    */
    function setParams($params){     
    
        
    
        if($params["templateName"]){
            $this->templateName=$params["templateName"]; 
        }
        else
        {
            $this->templateName=$this->templateNameDefault;
        }
    
        if($params["params"]){
            
            $this->err_no=intval($params["params"]["CODE"]);
            
        }
        
        //сохраняем вошедшие параметры
        $this->params=$params["params"];
        
    }
    
    /**
    * исполняет контроллер и выводит шаблон
    */    
    function execComp(){
        
        if($this->err_no){
            
            
            $this->textErr=\GlobalApp\ErrorsList::getErrText($this->err_no);
            
            $arResult=[
                "CODE"=>$this->err_no,
                "TEXT"=>$this->textErr.$this->params["MORE_INFO"]
            ];
            
            $this->getView($arResult);
            
        }
        
        
    }
    
    
    /**
    * выводит шаблон
    * @params array $arResult фссоциативный массив с данными, которые показываем в шаблоне
    */    
    function getView($arResult){
        
        //подключаем нужный шаблон и передаем в него результирующие данные
        if(is_file($this->tmplPath.$this->templateName."/template.php")){
            include($this->tmplPath.$this->templateName."/template.php");
        }
        
    }
    
}
