<?
namespace AddTask;

use \GlobalApp\ErrorsList;
use \GlobalApp\BaseCont;
use \TasksTable;

/** 
* Данный компонент служит для создания новой записи
* 
*/
class Controller extends BaseCont {
    
    function __construct (){
        
        global $App;
        
        $this->tmplPath=$App->getViewDir();
        
    }
        
    //шаблон контроллера по умолчанию
    var $templateNameDefault="add_task";
    //шаблон контроллера
    var $templateName="add_task";
    
    //режим работы a - добавление записи, v - отображение формы для сохранения
    var $mode="v";
    
    /**
    * Установка параметров для использования в контроллере и шаблоне
    */
    function setParams($params=[]){     
    
    
        if($params["templateName"]){
            $this->templateName=$params["templateName"]; 
        }
        else
        {
            $this->templateName=$this->templateNameDefault;
        }
    
        if($params["params"]){
            
            
        }
        
        if(isset($_REQUEST["add"])&&$_REQUEST["add"]=="y"){
            $this->mode="a";
        }
       
        //сохраняем вошедшие параметры
        $this->params=$params;
       
    }
    
    /**
    * исполняет контроллер и выводит шаблон
    */    
    function execComp(){
        
        global $App;
        if($App->includeModel("TasksTable")){
           
        
        
            //по сути у этого контроллера всего две задачи. 
            //1. Отобразить форму для внесения информации
            //2. Добавить новую запись
            //соответственно так и будем делать
            $this->arResult=[];
            $this->arResult["MODE"]=$this->mode;
            
            if($this->mode=="a"){
                
                
                
                //принимаем данные с запроса.
                //- имени пользователя;
                //- е-mail;
                //- текста задачи;
                
                $email =$_REQUEST["email"];
                $username =htmlentities(strip_tags($_REQUEST["username"]));
                $text =htmlentities(strip_tags($_REQUEST["text"],"<br><p><ul><li><ol><b><i><u>"));
                
                $errCheck=false;
                
                if (!filter_var($email, FILTER_VALIDATE_EMAIL)){                
                    //email bad
                    $errCheck=true;                
                    $this->arResult["ERRORS"][]=\GlobalApp\ErrorsList::getErrText(7);
                    
                }
                
                if(empty($username)){
                    $errCheck=true;                
                    $this->arResult["ERRORS"][]=\GlobalApp\ErrorsList::getErrText(8);
                }
                
                if(empty($text)){
                    $errCheck=true;                
                    $this->arResult["ERRORS"][]=\GlobalApp\ErrorsList::getErrText(9);
                }
            
                if(!$errCheck){
                    
                    $TasksTable=new TasksTable();
                    
                    $arToSave=[               
                        'USERNAME'=>$username,
                        'EMAIL'=>$email,
                        'TEXT'=>$text,
                        'STATUS'=>"W",//W - work, C - complited
                    
                    ];
                    
                    $resAdd=$TasksTable->add($arToSave);
                    $this->arResult["DATA"]=$resAdd;
                    
                    
                    
                }
                
            }
            else
            {
                $this->arResult["MODE"]=$this->mode;
            }
        }
        else
        {
           $this->arResult["ERRORS"][]=\GlobalApp\ErrorsList::getErrText(11); 
        }
        
        
        $this->getView($this->arResult);
        
    }
    
    
    /**
    * выводит шаблон
    * @params array $arResult ассоциативный массив с данными, которые показываем в шаблоне
    */    
    function getView($arResult){
        
        //подключаем нужный шаблон и передаем в него результирующие данные
        if(is_file($this->tmplPath.$this->templateName."/template.php")){
            
            include($this->tmplPath.$this->templateName."/template.php");
        }
        
    }
    
}
