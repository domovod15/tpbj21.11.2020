<?
namespace GlobalApp;

use \GlobalApp\ErrorsList;

/**
* Базовый класс работы с базой данных.
* Реализует главные методы работы с бд
*/
abstract class BaseMysql {
    
    //хранит имя таблицы
    protected $tableTame;
    //хранит список полей таблицы
    protected $mapList;
    
    //глобальный объект приложения. содержит ссылку на бд
    protected $App;
    
    function __construct(){
        
        global $App;
        $this->App=$App;
        
    }
    
    
    
    /**
    * Метод проверяет существование поля в таблице
    * @param $code string with name col in table
    */
    final function checkField($code){
        
        // лучше сделать мап таблицы в более расширенном виде с возможностью задания параметров проверки, фильтраци, обязательности
        
        if(in_array($code,$this->mapList)){
            return true;
        }
        
        return false;
        
    }
    
    /**
    * Метод добавляет данные в подчиненную таблицу
    * @params $params array список ключ=>зачение, где ключ- азвание колонки в бд
    * @return array $arRes["STATUS"=>false,"ID"=>false,"ERR_NO"=>false,"ERR_MESS"=false]
    */
    public function add($params){
        
        $dbLink=$this->App->dbLink;
        
        $keysParams=array_keys($params);
        
        
        foreach($keysParams as $oneKey){
            if(!$this->checkField($oneKey)){
                unset($params[$oneKey]);
            }
        }
        unset($keysParams);
        
        $arRes=[
                "STATUS"=>false,
                "ID"=>false,
                "ERR_NO"=>false,
                "ERR_MESS"=>false,
            ];
        
        if(count($params)>0){
            
            //создадим строки для запроса
            
            foreach($params as $key=>&$data){
                $data = '"'.$dbLink->real_escape_string($data).'"';
            }
             
            $querySql = "INSERT INTO ".$this->tableTame." (".implode(",",array_keys($params)).") VALUES(".implode(",",array_values($params)).")";

            $insertRes = $dbLink->query($querySql);
            
            if($insertRes){
                
                $arRes["STATUS"]=true;
                $arRes["ID"]=$dbLink->insert_id;
                
            }
            else{
                $arRes["ERR_NO"]=$mysqli->errno;
                $arRes["ERR_MESS"]=$dbLink->error;
            }

           
            
        }
        else
        {
            $arRes["ERR_NO"]=5;
            $arRes["ERR_MESS"]=\GlobalApp\ErrorsList::getErrText(5);
        }
       
        return $arRes;
        
    }
    
    /**
    * Метод обновляет данные в таблице по ключу ID
    * @params $id array ключ id записи в бд
    * @params $params array список ключ=>зачение, где ключ- азвание колонки в бд
    * @return array $arRes["STATUS"=>false,"ID"=>false,"ERR_NO"=>false,"ERR_MESS"=false]
    */
    public function update($id,$params){
        
        
        $dbLink=$this->App->dbLink;
        
        $keysParams=array_keys($params);
        
        foreach($keysParams as $oneKey){
            if(!$this->checkField($oneKey)){
                unset($params[$oneKey]);
            }
        }
        unset($keysParams);
        
        $arRes=[
                "STATUS"=>false,
                "ID"=>false,
                "ERR_NO"=>false,
                "ERR_MESS"=>false,
            ];
        
        if(intval($id)){
            if(count($params)>0){
                
                //создадим строки для запроса
                
                foreach($params as $key=>&$data){
                    $data = '"'.$dbLink->real_escape_string($data).'"';
                }
                
                //---------
                //подготовим данные
                $strRes="";
                    foreach($params as $k=>$v){
                        $strRes.=$k."=".$v.",";
                    }
                $strRes=trim($strRes,",");
                //------
                $querySql = "UPDATE ".$this->tableTame." SET ".(
                    $strRes
                )." WHERE ID=$id";
                
                $updateRes = $dbLink->query($querySql);
                
                if($updateRes){
                    
                    $arRes["STATUS"]=true;
                    $arRes["COUNT_ROWS"]=$dbLink->affected_rows;
                    
                }
                else{
                    $arRes["ERR_NO"]=$mysqli->errno;
                    $arRes["ERR_MESS"]=$dbLink->error;
                }
    
            }
            else
            {
                $arRes["ERR_NO"]=5;
                $arRes["ERR_MESS"]=\GlobalApp\ErrorsList::getErrText(5);
            }
        }
        else
        {
            $arRes["ERR_NO"]=6;
            $arRes["ERR_MESS"]=\GlobalApp\ErrorsList::getErrText(6);
        }
        return $arRes;
        
    }
    
    /**
    * Метод обновляет данные в таблице по ключу ID
    * @params $id array ключ id записи в бд
    * @return array $arRes["STATUS"=>false,"ID"=>false,"ERR_NO"=>false,"ERR_MESS"=false]
    */
    public function delete($id){
        
        
        $dbLink=$this->App->dbLink;
        
        $arRes=[
                "STATUS"=>false,
                "ID"=>false,
                "ERR_NO"=>false,
                "ERR_MESS"=>false,
            ];
        
        if(intval($id)){
            $querySql = "DELETE FROM ".$this->tableTame." WHERE ID=$id";
            
            $deleteRes = $dbLink->query($querySql);
            
            if($deleteRes){
                
                $arRes["STATUS"]=true;
                $arRes["COUNT_ROWS"]=$dbLink->affected_rows;
                
            }
            else{
                $arRes["ERR_NO"]=$mysqli->errno;
                $arRes["ERR_MESS"]=$dbLink->error;
            }
            
            $dbLink->free();
            
        }
        else
        {
            $arRes["ERR_NO"]=6;
            $arRes["ERR_MESS"]=\GlobalApp\ErrorsList::getErrText(6);
        }
        return $arRes;
        
    }
    
    /**
    *
    */
    public function getList($arSelect,$arFilter,$arOrder=["ID"=>"ASC"],$limit=10,$page=1){
        
        $dbLink=$this->App->dbLink;
        
        
        $arRes=[
                "STATUS"=>false,
                "ID"=>false,
                "ERR_NO"=>false,
                "ERR_MESS"=>false,
            ];
        
        //запрос будем формировать в несколько шагов.
        //каждый шаг будем отдельно обрабатывать
        $sqlQuery="";
        
        //create SELECT        
        //дополнительная проверка селектов и их обработка не нужна, поскольку мы сравниваем её с мапом таблицы
        foreach($arSelect as $i=>$oneKey){
            if(!$this->checkField($oneKey)){
                unset($arSelect[$i]);
            }
        }
        unset($i);
        if(count($arSelect)<=0){
            
            //если вдруг, как то так получилось, что количество колоно меньше нуля....или равно нулю) то просто всю таблицу извлекаем.
            $arSelect=$this->mapList;
            
        }
        //формируем строку
        
        if(!in_array("ID",$arSelect)){
            $arSelect[]="ID";//ID обязательное поле для всех таблиц
        }
        $selectStr="SELECT ".implode(",",$arSelect)." FROM ".$this->tableTame;
        
        //---------------------------------
        //create WHERE 
        // для создания фильтра заложу основные сравнения >,<,>=,<=,=
        // >,<,>=,<= задаётся путем префикса к ключу сравнения
        // = будем обрабатывать путём простого сравнения значения
        //теперь обрабатываем фильтр. В данной реализации у нас все в виде простого AND для всех передаваемых параметров
        $whereStr="";
        if(count($arFilter)>0){
           
            //массив обработанных значений, которые будем использовать для фильтрации в бд
            $whereArr=[];
            foreach($arFilter as $key=>$val){
                
                $f2=substr($key,0,2);
                $f1=substr($key,0,1);
                
                //итак, надо проверить значения
                //swith использовать не очень удобно, пока будет так
                if($f2==">="){
                    //получим сразу Имя поля
                    $fName=substr($key,2,strlen($key));
                    if(!$this->checkField($fName)){
                        continue;                        
                    }
                    //добавим в фильтр
                    $whereArr[]=$fName.">=".'"'.$dbLink->real_escape_string($val).'"';
                }
                else if($f2=="<="){
                    //получим сразу Имя поля
                    $fName=substr($key,2,strlen($key));
                    if(!$this->checkField($fName)){
                        continue;                        
                    }
                    //добавим в фильтр
                    $whereArr[]=$fName."<=".'"'.$dbLink->real_escape_string($val).'"';
                }
                else if($f1=="<"){
                    //получим сразу Имя поля
                    $fName=substr($key,1,strlen($key));
                    if(!$this->checkField($fName)){
                        continue;                        
                    }
                    //добавим в фильтр
                    $whereArr[]=$fName."<".'"'.$dbLink->real_escape_string($val).'"';
                }
                else if($f1==">"){
                    //получим сразу Имя поля
                    $fName=substr($key,1,strlen($key));
                    if(!$this->checkField($fName)){
                        continue;                        
                    }
                    //добавим в фильтр
                    $whereArr[]=$fName.">".'"'.$dbLink->real_escape_string($val).'"';
                }
                else if(!empty($val)){
                    
                    $fName=$key;     
                    
                    if(!$this->checkField($fName)){
                        continue;                        
                    }
                    //добавим в фильтр
                    $whereArr[]=$fName."=".'"'.$dbLink->real_escape_string($val).'"';
                    
                    
                    
                }
                else{
                    continue;
                }
                
            }
            
            $whereStr="WHERE ".trim(implode(" AND ",$whereArr)," AND ");
            
            
        }
        else{
            
            $whereStr="WHERE ID>0";// WHERE просто  чтобы не было пустым=)
            
        }

        //пожалуй, не самое удачное решение, но как вариант, для ограничения выборки делаю доп.запрос.
        // вообще количество затронутых строк можно получить позже и сделать проверку для пагинации
        //уже на пыхе. Но тогда нам можно скормить левые числа и бд будет их обрабатывать.
        //подход через доп.запрос потенциально хуже, но в данном случае я его оставлю.
        $allCountRowByWhere=$this->checkCountRows($whereStr);
        
        
        
        
        //---------------------------------
        //create ORDER 
        $orderStr="";
        foreach($arOrder as $fKey=>$fVal){
            if(!$this->checkField(strtoupper($fKey))){
                unset($arOrder[$fKey]);
            }
        }
        
        if(count($arOrder)==0){
            $orderStr="ID DESC";
        }
        else
        {
            //последним уровнем сортировки будет сортировка по ид
            //поэтому нужна доп проверка использовалась ли сортировка по ид ранее
            
            $checkIdSort=false;
            
            foreach($arOrder as $fKey=>$fVal){
                if(strtoupper($fVal)=="DESC"){
                    $orderStr.= strtoupper($fKey)." DESC,";
                }
                else
                {
                    $orderStr.= strtoupper($fKey)." ASC,";
                }
                
                
                if(strtoupper($fKey)=="ID"){
                    $checkIdSort=true;
                }
            }
            
            if(!$checkIdSort){
                $orderStr.= "ID ASC,";
            }
            
        }
        $orderStr="ORDER BY ".trim($orderStr,",");
        //---------------
        //create limit
        //чтобы запросы не пытались разорвать оперативку ограничим их лимитом
        if(intval($limit)){
            $limit=($limit>10000)?10000:intval($limit);
        }
        //и проверим какая страница в навигации нужна
        if(intval($page)){
            $page=intval($page)-1;//1 =1*10+10, т.е. 0*10+10 = 0-10; (2-1)*10+10= 10-20 и т.п.
            $limitStart=$page*$limit;
            //$limitStop=$page*$limit+$limit;
            $limitStop=$limit;
        }
        else
        {
            $page=0;
            $limitStart=$page*$limit;
            //$limitStop=$page*$limit+$limit;
            $limitStop=$limit;
        }
        
        if($allCountRowByWhere<$limitStop){
            $limitStop=$allCountRowByWhere;
        }
        
        
        $limitStr="LIMIT $limitStart,$limitStop";        
        //-----------------------
        //все данные проверены и подготовлены. Можно собирать запрос, если, конечно, есть смысл
        if($allCountRowByWhere>0){
            
            $strSql=$selectStr." ".$whereStr." ".$orderStr." ".$limitStr;
            $rowsRes = $dbLink->query($strSql);
            $rowsResultArray=[];
           
            
            
            if($rowsRes){
                
                while($row = $rowsRes->fetch_assoc()){
                    $rowsResultArray[$row["ID"]]=$row;
                }
                
                $arRes["STATUS"]=true;
                $arRes["COUNT_ROWS"]=count($rowsResultArray);
                $arRes["SQL_STR"]=$strSql;
                $arRes["ROWS_IN_SELECT"]=$allCountRowByWhere;
                $arRes["ITEMS"]=$rowsResultArray;
                $arRes["LIMIT"]=$limit;
                $arRes["LIMIT_END"]=$limitStop;
                $arRes["PAGE"]=$page+1;
                
            }
            else{
                $arRes["ERR_NO"]=$mysqli->errno;
                $arRes["ERR_MESS"]=$dbLink->error;
                $arRes["SQL_STR"]=$strSql;
                $arRes["STATUS"]=false;
                $arRes["ROWS_IN_SELECT"]=$allCountRowByWhere;
                $arRes["ITEMS"]=$rowsResultArray;
                $arRes["LIMIT"]=$limit;
                $arRes["LIMIT_END"]=$limitStop;
                $arRes["PAGE"]=$page+1;
            }
            
            if(method_exists($rowsRes,"free")){
                $rowsRes->free();
            }
            
            
        }
        else
        {
            $rowsResultArray=[];
            
            $arRes["STATUS"]=true;
            $arRes["ROWS_IN_SELECT"]=$allCountRowByWhere;
            $arRes["ITEMS"]=$rowsResultArray;
            $arRes["LIMIT"]=$limit;
            $arRes["LIMIT_END"]=$limitStop;
            $arRes["PAGE"]=$page;
        }
        
        return $arRes;
        
        
    }
    
    /**
    * метод проверяет сколько всего строк доступно под запрос
    * @param string $where строка, которая должна уже быть подготовлена для sql
    * @return int количество элементов в выборке
    */
    private function checkCountRows($where){
        
        $dbLink=$this->App->dbLink;
        
        $strSql="SELECT COUNT(*) as Q FROM ".$this->tableTame." $where";
        $countRes = $dbLink->query($strSql);
       
        if($countRes){
            
            if($row = $countRes->fetch_assoc()) {
                
                $countRes->free();
                
                return $row["Q"];
            }
            
        }
        
        return 0;
        
    }
    
    
}


































