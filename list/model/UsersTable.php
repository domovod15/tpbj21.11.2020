<?
use \GlobalApp\ErrorsList;
use \GlobalApp\BaseMysql;

/**
* Класс работы с таблицей задач.
* Реализует главные методы работы с задачами
*/
class UsersTable extends BaseMysql {
    
    //хранит имя таблицы
    protected $tableTame="users";
    //хранит список полей таблицы
    protected $mapList=[
       'ID',
       'USERNAME',
       'EMAIL',
       'PASSWORD',
       'HASH',
       'ACCESS',
    ];
    
    
    /**
    * Метод возвращает хеш для авторизованного пользователя
    */
    public function getHash(){
        
        $charsList = [
            "a","b","c","d","e",
            "f","g","h","i","j",
            "k","l","m","n","o",
            "p","q","r","s","t",
            "u","v","w","x","y",
            "z","A","B","C","D",
            "E","F","G","H","I",
            "J","K","L","M","N",
            "O","P","R","Q","S",
            "T","U","V","W","X",
            "Y","Z","0","1","2",
            "3","4","5","6",
            "7","8","9"
        ];
        
        shuffle($charsList);
        shuffle($charsList);
        
        $code=md5(implode("",array_slice($charsList,0, 10)));
        
        return $code;

        
    }
    
    
}


































