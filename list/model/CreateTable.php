<?
/**
* Просто класс для установки таблиц
*/
class CreateTable {

    function __construct(){
        
        global $App;
        $this->App=$App;
        
        
    }


    function setTables(){
        
        $arRes=[
                "STATUS"=>false,
                "ID"=>false,
                "ERR_NO"=>false,
                "ERR_MESS"=>false,
            ];
        
        $dbLink=$this->App->dbLink;
        
        
        //------------
        $strSql_t="
        CREATE TABLE IF NOT EXISTS `tasks` (
            `ID` int(11) NOT NULL auto_increment,
            `USERNAME` varchar(100) collate utf8_bin NOT NULL,
            `EMAIL` varchar(100) collate utf8_bin NOT NULL,
            `TEXT` text(2000) collate utf8_bin NOT NULL,
            `STATUS` varchar(1) collate utf8_bin default 'N',
            `CHECK_ADMIN` varchar(1) collate utf8_bin default 'N',
            PRIMARY KEY  (`ID`)
        ) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin ;
         ";
        
        $insertRes = $dbLink->query($strSql_t);
        
        if($insertRes){
            $arRes["tasks"]["STATUS"]=true;
            
        }
        else
        {
            $arRes["tasks"]["ERR_NO"]=$dbLink->errno;
            $arRes["tasks"]["ERR_MESS"]=$dbLink->error;
        }
        //--------
        $strSql_u="
        CREATE TABLE IF NOT EXISTS `users` (
            `ID` int(11) NOT NULL auto_increment,
            `USERNAME` varchar(100) collate utf8_bin NOT NULL,
            `EMAIL` varchar(100) collate utf8_bin NOT NULL,
            `PASSWORD` varchar(100) collate utf8_bin NOT NULL,
            `HASH` varchar(50) collate utf8_bin NOT NULL,
            `ACCESS` varchar(1) collate utf8_bin default 'N',
            PRIMARY KEY  (`ID`)
        ) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin ;
        ";
        $insertRes = $dbLink->query($strSql_u);
        
        if($insertRes){
            $arRes["users"]["STATUS"]=true;
            
        }
        else
        {
            $arRes["users"]["ERR_NO"]=$dbLink->errno;
            $arRes["users"]["ERR_MESS"]=$dbLink->error;
        }
        
        
        return $arRes;
        
    }



}