<div class="">

    <?
    if($arResult["ERRORS"]){
        foreach($arResult["ERRORS"] as $er){
            ?>
            <div class="alert alert-danger">
                <?
                echo $er;
                ?>
            </div>
            <?
        }
    }
    if($arResult["DATA"]["ERR_MESS"]){
        ?>
        <div class="alert alert-danger">
            <?
           echo  $arResult["DATA"]["ERR_MESS"];
            ?>
        </div>
        <?
    }
    ?>
    <?
    if($arResult["MODE"]=="w"){
        
        if($arResult["RES_UPD"]["STATUS"]==1){
        ?>
        <div class="alert alert-success">
            Обновление прошло успешно
        </div>
        <?
        }
    }
    ?>
    
    
    <div class="col-xs-6">
        <?
        if(count($arResult["DATA"]["ITEMS"])>0){
            
            foreach($arResult["DATA"]["ITEMS"] as $id=>$row){
            ?>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Пользователь:<?=$row["USERNAME"]?><br>
                        EMAIL:<?=$row["EMAIL"]?><br>
                        Статус:<?=$row["STATUS"]?>
                    </div>
                    
                    <div class="panel-body">
                        <?=html_entity_decode($row["TEXT"])?>
                    </div>
                </div>
            
            <?
            }//end foreach
            
        }//end if count
        
        ?>
    </div>
    <div class="col-xs-6">
        <?
        if($arResult["ACCESS"]=="Y"){
            if(count($arResult["DATA"]["ITEMS"])>0){
                
                foreach($arResult["DATA"]["ITEMS"] as $id=>$row){
                ?>
                    
                    
                    <form class="form-horizontal" role="form" method="post">
        
                        <div class="form-group">
                            <label for="inputEmail" class="col-sm-4 control-label">Email</label>
                            <div class="col-sm-8">
                                <input type="email" name="email" value="<?=$row["EMAIL"]?>" class="form-control" id="inputEmail" placeholder="Email">
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label for="inputUser" class="col-sm-4 control-label">Имя пользователя</label>
                            <div class="col-sm-8">
                                <input type="text" name="username" value="<?=$row["USERNAME"]?>" class="form-control" id="inputUser" placeholder="Имя пользователя">
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label for="status" class="col-xs-4 control-label">Имя пользователя</label>
                            <div class="col-sm-8">
                                <select class="form-control" name="status" id="status">
                                    <option value="W" <?=($row["STATUS"]=="W")?"selected":""?>>в работе</option>
                                    <option value="C" <?=($row["STATUS"]=="C")?"selected":""?>>выполнена</option>
                                </select>
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label class="col-xs-12 control-label" for="text">
                                Вы можете использовать теги:&lt;br&gt;&lt;p&gt;&lt;ul&gt;&lt;li&gt;&lt;ol&gt;&lt;b&gt;&lt;i&gt;&lt;u&gt;
                            </label>
                            <div class="col-sm-12">
                                <textarea class="form-control" rows="3" name="text" id="text"><?=html_entity_decode($row["TEXT"])?></textarea>
                            </div>
                        </div>
                        
                        
                        <input type="hidden" name="update" value="y">
                        <input type="hidden" name="id" value="<?=$row["ID"]?>">
                        <button type="" class="btn btn-default" >Изменить</button>
                        
                        
                        <div class="alert alert-info">
                            Сообщение "Изменено администратором" появится если внести изменения в поля: EMAIL/USERNAME/TEXT/STATUS
                        </div>
                    </form>
                    
                <?
                }//end foreach
                
            }//end if count
        }
        else
        {
            ?>
            <div class="alert alert-info" role="alert">
                Для редактирования записи нужно авторизоваться
            </div>
            <?
        }
        ?>
    </div>
</div>
