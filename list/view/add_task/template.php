<div class="">
    <?
    if(isset($arResult["ERRORS"])){
        foreach($arResult["ERRORS"] as $er){
            ?>
            <div class="alert alert-danger">
                <?
                echo $er;
                ?>
            </div>
            <?
        }
    }
    //вообще в зависимости от мода работы можно подключать разные шаблоны, или вообще не подключать
    if($arResult["MODE"]=="a"){
        
        //Проверяем ответ от результата добавления
        if(isset($arResult["DATA"])){
            
            if($arResult["DATA"]["STATUS"]){
            ?>
                <div class="alert alert-success">
                    Запись добавлена. Ид вашей задачи:<?=$arResult["DATA"]["ID"]?>
                </div>
            <?
            }
        }
        else
        {
            ?>
            <div class="alert alert-danger">
                Ошибка добавления записи.<br>
                <button type="button" class="btn btn-default" onclick="location.href=location">Попробуйте еще</button>
            </div>
            <?
        }
        
        
    }
    else
    {
        ?>
        
        <form class="form-horizontal" role="form" method="post">
        
            <div class="form-group">
                <label for="inputEmail" class="col-sm-2 control-label">Email</label>
                <div class="col-sm-10">
                    <input type="email" name="email"  class="form-control" id="inputEmail" placeholder="Email">
                </div>
            </div>
            
            <div class="form-group">
                <label for="inputUser" class="col-sm-2 control-label">Имя пользователя</label>
                <div class="col-sm-10">
                    <input type="text" name="username" class="form-control" id="inputUser" placeholder="Имя пользователя">
                </div>
            </div>
            
            <div class="form-group">
                <label class="col-sm-12 control-label" for="text">Вы можете использовать теги:&lt;br&gt;&lt;p&gt;&lt;ul&gt;&lt;li&gt;&lt;ol&gt;&lt;b&gt;&lt;i&gt;&lt;u&gt;</label>
                <div class="col-sm-12">
                    <textarea class="form-control" rows="3" name="text" id="text"></textarea>
                </div>
            </div>
            
            
            <input type="hidden" name="add" value="y">
            <button type="" class="btn btn-default" >Добавить</button>
            
        </form>

        <?
    }
    ?>
</div>