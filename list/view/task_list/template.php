<div class="">
    <?
    if(@$arResult["ERRORS"]){
        foreach($arResult["ERRORS"] as $er){
            ?>
            <div class="alert alert-danger">
                <?
                echo $er;
                ?>
            </div>
            <?
        }
    }
    ?>
    <div class="col-xs-12">
               
        <?
        if(count($arResult["DATA"]["ITEMS"])>0){
            ?>
            <div class="table-responsive">
                <table class="table">
                    <thead>
                        <tr>
                            <?
                            if(@$arResult["DATA"]["SORT_PARAM"]["id"]["actual"]){
                                $trend=$arResult["DATA"]["SORT_PARAM"]["id"]["trend"];
                                ?>
                                <th>
                                    <a href="<?=$arResult["DATA"]["SORT_PARAM"]["id"]["url"]?>">
                                        ID
                                        <?=($trend=="up")?"&uarr;":"&darr;"?>
                                    </a>
                                </th>
                                <?
                            }
                            else
                            {
                                $trend=$arResult["DATA"]["SORT_PARAM"]["id"]["trend"];
                                ?>
                                <th>
                                    <a href="<?=$arResult["DATA"]["SORT_PARAM"]["id"]["url"]?>">
                                        ID
                                    </a>
                                </th>
                                <?
                            }
                            ?>
                            
                            <?
                            if(@$arResult["DATA"]["SORT_PARAM"]["username"]["actual"]){
                                $trend=$arResult["DATA"]["SORT_PARAM"]["username"]["trend"];
                                ?>
                                <th>
                                    <a href="<?=$arResult["DATA"]["SORT_PARAM"]["username"]["url"]?>">
                                        Имя пользователя
                                        <?=($trend=="up")?"&uarr;":"&darr;"?>
                                    </a>
                                </th>
                                <?
                            }
                            else
                            {
                                $trend=$arResult["DATA"]["SORT_PARAM"]["username"]["trend"];
                                ?>
                                <th>
                                    <a href="<?=$arResult["DATA"]["SORT_PARAM"]["username"]["url"]?>">
                                        Имя пользователя
                                    </a>
                                </th>
                                <?
                            }
                            ?>
                           
                            <th>Emal</th>
                            <?
                            if(@$arResult["DATA"]["SORT_PARAM"]["status"]["actual"]){
                                $trend=$arResult["DATA"]["SORT_PARAM"]["status"]["trend"];
                                ?>
                                <th>
                                    <a href="<?=$arResult["DATA"]["SORT_PARAM"]["status"]["url"]?>">
                                        Статус
                                        <?=($trend=="up")?"&uarr;":"&darr;"?>
                                    </a>
                                </th>
                                <?
                            }
                            else
                            {
                                $trend=$arResult["DATA"]["SORT_PARAM"]["status"]["trend"];
                                ?>
                                <th>
                                    <a href="<?=$arResult["DATA"]["SORT_PARAM"]["status"]["url"]?>">
                                        Статус
                                    </a>
                                </th>
                                <?
                            }
                            ?>
                            <th>Текст</th>
                            <th>Ред. адм.</th>
                            <th>Действие</th>
                        </tr>
                    </thead>
                
                    <?
                    foreach($arResult["DATA"]["ITEMS"] as $id=>$row){
                    ?>
                        <tr>
                            <td><?=$row["ID"]?></td>
                            <td><?=$row["USERNAME"]?></td>
                            <td><?=$row["EMAIL"]?></td>
                            <td><?=$row["STATUS"]?></td>
                            <td><?=substr($row["TEXT"],0,30)?>...</td>
                            <td><?=($row["CHECK_ADMIN"]=="Y")?"да":"нет"?></td>
                            <td>
                                <a href="<?=$row["URL_VIEW"]["STRING"]?>">
                                    просмотр
                                </a>
                            </td>
                        </tr>
                    
                    <?
                    }//end foreach
                ?>
                </table>
            </div>
            <?
            if(@$arResult["DATA"]["PAGINATION"]){
                ?>
                <ul class="pagination">
                    <?
                    if(@$arResult["DATA"]["PAGINATION"]["PAGES"]["FIRST"]){
                        ?>
                        <li>
                            <a href="?<?=$arResult["DATA"]["PAGINATION"]["PAGES"]["FIRST"]["URL_PARAM"]?>">
                                <?=$arResult["DATA"]["PAGINATION"]["PAGES"]["FIRST"]["NUMBER"]?>
                            </a>
                        </li>
                        <?
                    }
                    ?>
                    <?
                    if(@$arResult["DATA"]["PAGINATION"]["PAGES"]["PREVPAGE"]){
                        ?>
                        <li>
                            <a href="?<?=$arResult["DATA"]["PAGINATION"]["PAGES"]["PREVPAGE"]["URL_PARAM"]?>">
                                <?=$arResult["DATA"]["PAGINATION"]["PAGES"]["PREVPAGE"]["NUMBER"]?>
                            </a>
                        </li>
                        <?
                    }
                    ?>
                    <?
                    if(@$arResult["DATA"]["PAGINATION"]["PAGES"]["ACTUAL"]){
                        ?>
                        <li class="disabled">
                            <a href="?<?=$arResult["DATA"]["PAGINATION"]["PAGES"]["ACTUAL"]["URL_PARAM"]?>">
                                <?=$arResult["DATA"]["PAGINATION"]["PAGES"]["ACTUAL"]["NUMBER"]?>
                            </a>
                        </li>
                        <?
                    }
                    ?>
                    <?
                    if(@$arResult["DATA"]["PAGINATION"]["PAGES"]["NEXTPAGE"]){
                        ?>
                        <li>
                            <a href="?<?=$arResult["DATA"]["PAGINATION"]["PAGES"]["NEXTPAGE"]["URL_PARAM"]?>">
                                <?=$arResult["DATA"]["PAGINATION"]["PAGES"]["NEXTPAGE"]["NUMBER"]?>
                            </a>
                        </li>
                        <?
                    }
                    ?>
                    <?
                    if(@$arResult["DATA"]["PAGINATION"]["PAGES"]["LAST"]){
                        ?>
                        <li>
                            <a href="?<?=$arResult["DATA"]["PAGINATION"]["PAGES"]["LAST"]["URL_PARAM"]?>">
                                <?=$arResult["DATA"]["PAGINATION"]["PAGES"]["LAST"]["NUMBER"]?>
                            </a>
                        </li>
                        <?
                    }
                    ?>
                </ul>

                
                <?
            }
            
            ?>
        <?
        }//end if count
        else
        {
            ?>
            <div class="alert alert-danger">
                Элементы для отображения не найдены
            </div>
            <?
        }
        
        ?>
    </div>
</div>
