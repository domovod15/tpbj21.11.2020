<?
$obContentHeader=ob_get_contents();
ob_get_clean();
  
echo $obContentHeader;
?>

<div class="">

   
    <?
        
    if(isset($arResult["ERRORS"])){
        foreach($arResult["ERRORS"] as $er){
            ?>
            <div class="alert alert-danger">
                <?
                echo $er;
                ?>
            </div>
            <?
        }
    }
    
    
    //Проверяем ответ от результата добавления
    if(isset($arResult["MESS"])&&$_REQUEST["auth"]=="out"){
        ?>
        <div class="alert alert-success">
            Авторизация закончена
        </div>
        <?
    }
    if(isset($arResult["MESS"])&&$_REQUEST["auth"]=="y"){
        ?>
        <div class="alert alert-success">
            Авторизация выполнена
        </div>
        <?
    }
    
    if(!isset($arResult["DATA"]["ID"]))
    {
        ?>
        
        <form class="form-horizontal" role="form" method="post">
        
            <div class="form-group">
                <label for="inputUser" class="col-sm-2 control-label">Имя пользователя</label>
                <div class="col-sm-10">
                    <input type="text" name="username" class="form-control" id="inputUser" placeholder="Имя пользователя">
                </div>
            </div>
            
            <div class="form-group">
                <label for="inputpass" class="col-sm-2 control-label">Пароль</label>
                <div class="col-sm-10">
                    <input type="password" name="password"  class="form-control" id="inputpass" placeholder="Пароль">
                </div>
            </div>
            
            <input type="hidden" name="auth" value="y">
            <div class="form-group">
                <div class="col-sm-offset-8 col-sm-4">
                    <button type="" class="btn btn-default" >войти</button>
                </div>
            </div>
            
            
        </form>

        <?
    }
    else
    {
        ?>
        <p>
            Ид: <?=$arResult["DATA"]["ID"]?><br>
            Имя пользователя: <?=$arResult["DATA"]["USERNAME"]?><br>
            Хеш авторизации: <?=$arResult["DATA"]["HASH"]?><br>
           
        </p>
        <p>
            <form class="form-horizontal" role="form" method="post">
                <input type="hidden" name="auth" value="out">
                <button type="" class="btn btn-default" >Выйти</button>            
            </form>
        </p>
        <?
    }
    ?>
</div>