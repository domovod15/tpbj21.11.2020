<?
//начинаем с того, что создаем базовый объект приложения.
//Он установит соединение с бд, проставит пути, позволи в дальнейшем подключать разные контроллеры/компоненты
ini_set('error_reporting', E_ALL);
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);


global $App;
require_once(__DIR__."/classes/BaseApp.php");
use GlobalApp\BaseApp;


$App=new BaseApp();
$App->loadSettingApp();


//включаем хедер
$App->includeComponent("start_page",$templateName="start_page",[]);
?>
<div class="container">
    <div class="row">
        <div class="col-xs-12">
            <?
            //включаем котнроллер ответственный создание задачи
            $App->includeComponent("task_list",$templateName="task_list",[
                "PATH2EDIT"=>"add.php?id=#ID#",
                "PATH2VIEW"=>"view.php?id=#ID#",
                "LOCALPATH"=>"index.php",
                "PAGE_SIZE"=>@$_GET["page_size"],
                "SORT"=>@[$_GET["by"]=>$_GET["order"]],
                "PAGE_NUMBER"=>@$_GET["page_number"]
            
            ]);
           ?>
        </div>
    </div>
</div>
<?

//включаем footer
$App->includeComponent("end_page",$templateName="end_page",[]);
//закрываем соединение с бд
$App->closeMysql();
?>